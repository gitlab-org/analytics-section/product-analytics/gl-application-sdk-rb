# frozen_string_literal: true

RSpec.describe GitlabSDK do
  it "has a version number" do
    expect(GitlabSDK::VERSION).not_to be nil
  end
end
