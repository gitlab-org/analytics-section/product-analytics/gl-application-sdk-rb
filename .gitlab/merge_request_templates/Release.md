<!-- Replace `<PREVIOUS_VERSION>` with the previous version number, `<COMMIT_UPDATING_VERSION>` with the latest
commit from this merge request, and `<NEW_VERSION>` with the upcoming version number. -->
## Diff

https://gitlab.com/gitlab-org/analytics-section/product-analytics/gl-application-sdk-rb/-/compare/v<PREVIOUS_VERSION>...<COMMIT_UPDATING_VERSION>

## Checklist

- [ ] Change the `VERSION` constant to a version in `lib/gitlab-sdk/version.rb` according to [SemVer](https://semver.org).
- [ ] Run `bundle install` to update the version in the `Gemfile.lock` file.
- [ ] Ensure the diff link above is up-to-date.
- [ ] Add release notes to the [Changelog](#changelog) section below.

## Changelog

<!--
If you do not have `jq`, install it with `brew install jq`

Then, paste output of:

curl https://gitlab.com/api/v4/projects/44795183/repository/changelog?version=<NEW_VERSION> | jq -r ".notes"

NOTE: Skip `v` in `<NEW_VERSION>`. For example, Use `version=10.0.0` instead of `version=v10.0.0`.

-->

/label ~"type::maintenance"
